#include <iostream>

using namespace std;

int main()
{
    /* Este programa suma todos los numeros amigables menores al numero ingresado por el usuario por medio de dos ciclos anidados,
    y hallando los divisores de cada numero para verificar todas las combinaciones de numeros amigables posibles*/

    int numeroIngresado,sumaDivisores=0, sumaAmigables=0;
    cout << "Ingrese un numero: " << endl;
    cin >> numeroIngresado;

    if (numeroIngresado > 0){
        cout <<"La suma de los numeros amigables menores a "<<numeroIngresado<<" es: "<<endl;
        for (int primerNumeroAmigable= 1; primerNumeroAmigable <= numeroIngresado; primerNumeroAmigable++){     //Ciclo que sera el primer valor que se comparara con el contador del segundo ciclo

            for (int segundoNumeroAmigable = 1; segundoNumeroAmigable <= numeroIngresado; segundoNumeroAmigable++ ){   //Ciclo que sera el segundo valor que se compara con el contador del primer ciclo

                for (int divisores = 1;divisores < segundoNumeroAmigable; divisores++){  //Ciclo que saca el numero de divisores del numero segundoNumeroAmigable

                    if (segundoNumeroAmigable % divisores == 0){    //Si al dividir el numero del primer ciclo por el contador divisores da como residuo 0, es un divisor
                        sumaDivisores+=divisores;           //Si es divisor, se agrega a la suma de divisores del numero dado

                    }
                }

                if (sumaDivisores == primerNumeroAmigable){        //Si la suma de divisores es igual al numero dado, es un numero amigable
                    sumaAmigables += segundoNumeroAmigable;         //Al ser numero amigable, se agrega a la suma de los numeros amigables la pareja de numeros amigables
                    sumaAmigables+=primerNumeroAmigable;
                    cout << primerNumeroAmigable<<" + "<< segundoNumeroAmigable << " = "<<sumaAmigables<<endl;
                }
                sumaDivisores=0;    //Reinicio de la variable sumaDivisores para sacar la suma de los divisores del siguiente numero
                sumaAmigables=0;    //Reinicio de la variable sumaAmigables para poder obtener la suma unicamente por parejas
            }
        }

    }
    else
        cout <<"Ingrese un numero positivo. "<<endl;

    return 0;
}
